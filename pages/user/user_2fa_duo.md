---
title: Setup Duo two-factor-authentication
tags: [2FA, userguide]
summary: "How to protect your account with Duo two-factor-authentication"
sidebar: user_sidebar
permalink: user_2fa_duo.html
folder: user
---

## Preamble

Another well known two-factor-authenticator is Duo. We strongly recommend setting up such a 2-Factor Authentication to
protect your account. Please generate a recovery code afterwards so you always have a possibility to gain access to your account in case
you lose your mobile.

## Pre-requirements

Duo is a little bit more work to setup, as it has the precondition that you register for a Duo Account first on [duo.com](https://duo.com/).

During the registration process you are "forced" to download the "Duo Mobile" App, that you can get here for
[Android](https://play.google.com/store/apps/details?id=com.duosecurity.duomobile) or here for
[Apple](https://itunes.apple.com/us/app/duo-mobile/id422663827?mt=8)


## Setup Guide

First we will now create a new application in the Duo dashboard, and afterwards we will configure Psono to use the appropriate
"Integration Key" and "Secret Key".

### Setup Duo

1) Login to Duo.

2) Go to "Applications" and click "Protect an Application"

![Step 2 Protect an Application](images/user/2fa_duo/step-2-create-new-application.jpg)

3) Select "Auth API" and click "Protect this Application"

![Step 3 Select Auth API](images/user/2fa_duo/step-3-select-auth-api.jpg)

4) Write down all three parameters (Integration key, secret key and API hostname) from the "Details" section.

![Step 4 Select Auth API](images/user/2fa_duo/step-4-write-down-credentials.jpg)

5) Scroll down and specify a "name" and click "Save Changes"

![Step 5 Specify name and click save](images/user/2fa_duo/step-5-click-save.jpg)

### Setup Psono

1)  Login into your account:

![Step 1 Login](images/user/2fa_yubikey/step1-login.jpg)

2)  Go to "Account":

![Step 2 go to account](images/user/2fa_yubikey/step2-go-to-account.jpg)

3)  Select the “Multifactor Authentication” tab:

![Step 3 go to multifactor authentication](images/user/2fa_yubikey/step3-go-to-multifactor-authentication.jpg)

4)  Click the "Configure" button next to "Duo":

![Step 4 click configure next to Google Authenticator](images/user/2fa_google_authenticator/step4-click-configure-next-to-google-authenticator.jpg)

5)  Click the "New Duo" tab:

![Step 5 select new Duo](images/user/2fa_duo/step5-click-new-duo.jpg)

6)  Type in a name and the details that we have saved before and confirm everything with "Setup":

![Step 6 add details](images/user/2fa_google_authenticator/step6-add-details.jpg)

7)  Scan the QR Code with your App:

![Step 7 scan qr code](images/user/2fa_duo/step7-scan-qr-code.jpg)

8) Approve the push message on your phone or add a correct Duo code for validation:

![Step 8 Validate Duo](images/user/2fa_duo/step-8-validate-duo.jpg)

Well done, your Duo is now active. We strongly recommend setting up a recovery code in case your phone gets damaged, is lost or stolen.

{% include links.html %}
