---
title: Entity Model
tags: [development, entity model]
summary: "The overview of the entity model"
sidebar: mydoc_sidebar
permalink: mydoc_development_entity_model.html
folder: mydoc
---

Below you will find a brief explanation of the Psono entity model.
The three main "entities" and their meaning:

- Datastores
- Secrets
- Shares


## Datastore

Every user owns one datastore of every type. Currently there are 3 types.
The password datastore, the user datastore and the settings datastore.

Every datastore is encrypted with a unique symmetric key, that is stored
symmetric encrypted by the users secret_key on the server.

##### Datastore Setting

The settings datastore contains the basic settings of the user, like e.g.
his defaults to generate a passwords.

##### Datastore User

The user datastore contains the user details of other users, that he added
as "trusted" users, especially the public_key of the other user.

##### Datastore Password

The password datastore is the main datastore, holding the entryoint for the
user's passwords. It contains a filestructure like described here in the
[Datastore Structure Documentation](mydoc_development_datastore_structure.html)

## Secret

Secrets are the containers for anything sensible. A secret is always linked
to in a datastore or a share (see below). The  secret_key to decrypt the
secret is part part of the informations stored in the datastore / share.
The JSON structure of the secret is flexible and depends on the type of the
secret (e.g. note, password, ...)

Every secret is encrytped with a unique symmetric key, that is stored in
plain text in the parent object (either datastore or share) that links to
the secret.

The server keeps track of the location of the secrets with the "Secret Link"
Model.

## Share

Shares are the base object that is created whenever a user wants to share
anything with another user. They can contain a complete filestructures,
similar to a datastore (described in the
[Datastore Structure Documentation](mydoc_development_datastore_structure.html)) with links to secrets.
The main difference in comparison to datastores is that shares can be
accessed by multiple users, based on their access rights.

Every share is encrytped with a unique symmetric key, that is stored in plain
text in the parent object (either datastore or share) that links to the share.

The server keeps track of the Share structure in the "Share Tree" Model.

## Entities as a diagram

A typical entity structure can look like this:

![Entity relation diagram](images/Entity_relation.png)

Two users, each of them owning their three datastores. The password Datastores
can link multiple shares and multiple secrets. One share can link multiple other
shares and multiple other secrets.

## Permission system

### Datastore

Every datastore is owned by exactly one user. That user has full access privileges and can create, delete, modify it as he / she wants.

### Shares

Every share has a defined set of permissions for every user ("User Share Right") and group ("Group Share Right").

The possible permissions are:

- Read: Allows a user / group to read the content of a share
- Write: Allows a user / group to modify the content of a share
- Grant: Allows the user / group to modify the access permissions (including his own) and share it with other users / groups.


Some additional infos:

- Share rights are inherited. Thats why we distinguish "direct permissions" from "inherited permissions".
Let's take as an example the diagram above. If a user has a right for "Share 2", then he automatically has the same
rights for "Share 3"

- "Direct permissions" can be "User Share Rights" or "Group Share Rights". "Inherited permissions" therefore can come
from "User Share Rights" or "Group Share Rights"

- "Direct permissions" beat "inherited permissions". If a user has "direct permissions" that do not allow "write" on "Share 2",
then inheritance wise, he would also not have "write" permissions on "Share 3". But. If someone gave him direct
permissions on "Share 3" to write, then he can write. This works also for removing permissions.

- "Deleting" a share does not actually delete a share. It only removes the link from the parent to the share.

- The right to "delete" a share, comes from the "write" permission of the parent, as it would mean removing the link
from the parent and therefore modifying it. In the diagram above both users can "delete" "Share 2".
If a user would try to delete "Share 3", then it would depend on the "write" permissions he has on "Share 2" (even if he has all
direct permissions for "Share 3")

### Secrets

Access to secrets is based on the permissions on the entity (datastore or share) where they reside.

{% include links.html %}

