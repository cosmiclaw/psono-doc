---
title: V1 Architecture
tags: [asvs, owasp]
summary: "Architecture, design and threat modelling"
sidebar: mydoc_sidebar
permalink: mydoc_asvs_v1_architecture.html
folder: mydoc
---


## ASVS Verification Requirement

| ID                          | Detailed Verification Requirement                                                                                                                                                                                                                                                                                                                              | Level 1                 | Level 2    | Level 3     | Since     | 
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|------------|-------------|-----------| 
| 1.1                         | Verify that all application components are identified and are known to be needed.                                                                                                                                                                                                                                                                              | x                       | x          | x           | 1.0       | 
| 1.2                         | Verify that all components, such as libraries, modules, and external systems, that are not part of the application but that the application relies on to operate are identified.                                                                                                                                                                               |                         | x          | x           | 1.0       | 
| 1.3                         | Verify that a high-level architecture for the application has been defined.                                                                                                                                                                                                                                                                                    |                         | x          | x           | 1.0       | 
| 1.4                         | Verify that all application components are defined in terms of the business functions and/or security functions they provide.                                                                                                                                                                                                                                  |                         |            | x           | 1.0       | 
| 1.5                         | Verify that all components that are not part of the application but that the application relies on to operate are defined in terms of the functions, and/or security functions, they provide.                                                                                                                                                                  |                         |            | x           | 1.0       | 
| 1.6                         | Verify that a threat model for the target application has been produced and covers off risks associated with Spoofing, Tampering, Repudiation, Information Disclosure, and Elevation of privilege (STRIDE).                                                                                                                                                    |                         |            | x           | 1.0       | 
| 1.7                         | Verify all security controls (including libraries that call external security services) have a centralized implementation.                                                                                                                                                                                                                                     |                         | x          | x           | 1.0       | 
| 1.8                         | Verify that components are segregated from each other via a defined security control, such as network segmentation, firewall rules, or cloud based security groups.                                                                                                                                                                                            |                         | x          | x           | 3.0       | 
| 1.9                         | Verify the application has a clear separation between the data layer, controller layer and the display layer, such that security decisions can be enforced on trusted systems.                                                                                                                                                                                 |                         | x          | x           | 3.0       | 
| 1.10                        | Verify that there is no sensitive business logic, secret keys or other proprietary information in client side code.                                                                                                                                                                                                                                            |                         | x          | x           | 3.0       | 
| 1.11                        | Verify that all application components libraries modules frameworks platform and operating systems are free from known vulnerabilities.                                                                                                                                                                                                                        |                         | x          | x           | 3.0.1     | 


## Evaluation

### V1.1

| Component | Required? | Details |
| --------- | ----------------- | ----------------- |
| Psono Server | needed | middleware that takes all connections |
| Postgres Database | needed | cannot save data without |
| Reverse Proxy | needed | ssl offload and security enhancement |
| Psono Client | optional | only required if someone wants to use only browser extensions |
| Psono Admin Client | optional | only required if someone wants a graphical UI to administrate the server |
| Browser Extensions | optional | only required if someone wants to use only web client |
| Cache | optional | only required for higher throughput or multiple psono server instances |

### V1.2

| Component | Required? | Details |
| --------- | ----------------- | ----------------- |
| E-Mail Server | needed | sends registration emails |
| Time Server | needed | syncs time across instances |
| Duo API | optional | only required if someone wants to use duo 2FA |
| Yubico API | optional | only required if someone wants to use yubikey 2FA |
| Cloudflare | optional | only required for DDoS protection |
| GCP | needed | Provides the infrastructure |
| DNS Server | needed | Required to resolve e.g. yubikey or duo API servers |
| Psono License Server | needed | creates licenses for offline and online use case |


### V1.3

![Overview of a typical setup](images/setup_typical.png)

// TODO Add Application Diagram

### V1.4

| Component | Business function | Security function |
| --------- | ----------------- | ----------------- |
| Psono Server | Backend business logic<br>Middleware to the database | Rate limiting<br>Access Control<br>Transport Encryption Server<br>Replay protection |
| Postgres Database | Data storage | Data backups<br>HA |
| Reverse Proxy | Glues client and server together<br> Cache Control | SSL Offload<br>IFrame Prevention<br>XSS Protection<br>CSS Protection |
| Psono Client | Access for users to passwords without any installation requirement | Encrypts all data<br>Transport Encryption Client |
| Psono Admin Client | Administration of the server<br>User Management | Version Check of all components<br>Health check of all components |
| Browser Extensions | Access for users to passwords<br>Extends functionality<br>Additional convenience | Encrypts all data<br>Transport Encryption Client |
| Cache | Base for scalability<br>HA requirement | - |


### V1.5

| Component | Business function | Security function |
| --------- | ----------------- | ----------------- |
| E-mail server | Delivers registration e-mails<br>Delivers security notifications | E-mail verification<br>Channel for security notifications |
| Time server | Synchronizes the time across all servers | Correct logging time<br>Google Authenticator Code validation<br>Replay protection |
| Duo API | Access to accounts with 2FA | Second factor for login |
| Yubico API | Access to accounts with 2FA | Second factor for login |
| Cloudflare | CDN capability | DDoS protection<br>SSL Endpoint for clients |
| GCP | Provides the infrastructure | Backup |
| DNS | Capability to synchronize time or use external services e.g. Duo | Second Factor, Logging |
| Psono License Server | Creates offline and online licenses | Rate limiting<br>Signs licenses |

### V1.6

Check [STRIDE analysis](mydoc_asvs_stride).


### V1.7

| Component | Central Implementation |
| --------- | ---------------------- |
| Logging Component | Yes |
| Client Crypo library | No, web client and admin portal implementation are distinct. |
| Duo library | Yes |
| GA library | Yes |
| Yubico library | Yes |
| GCP Firewall / Network | Yes |


### V1.8

| Component       | Segmentation                                                                                                                       |
| --------------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| Database        | Yes, own network zone, only server IPs whitelisted.                                                                                |
| Reverse Proxy   | Segmented from Server and Clients only by docker, can be improved, but not done due to costs.                                      |
| Server          | Segmented from Reverse Proxy and Clients only by docker, can be improved, but not done due to costs. Blocked from internet access. |
| Webclient       | Segmented from Server and Reverse Proxy only by docker, can be improved, but not done due to costs. Blocked from internet access.  |
| Admin Webclient | Segmented from Server and Reverse Proxy only by docker, can be improved, but not done due to costs. Blocked from internet access.  |
| Cache           | Yes, own server, but same network zone as Reverse Proxy and Server. Blocked from internet access.                                  |
| Extension       | Yes, runs on the client computer.                                                                                                  |

### V1.9

Given by the REST design

| Component       | Segmentation                                                  |
| --------------- | ------------------------------------------------------------- |
| Database        | Data Layer, stores data                                       |
| Client(s)       | Display Layer, responsible to display the data.               |
| Server          | Controller Layer, REST API with the backend logic implemented |


### V1.10

No client contains any secret. Secrets are pulled from the server once the user authenticates.

### V1.11

Multiple measures are in place to make sure that all available security updates are applied on a regular basis:

| Component           | Segmentation                                                  |
| ------------------- | ------------------------------------------------------------- |
| Postgres            | Provided by GCP, Googles responsibility                       |
| VM                  | Automated security updates are enabled                        |
| Docker Images       | Automated security scan by anchore.io                         |
| NPM libraries       | Automated vulnerability check through github.com              |
| Python libraries    | Automated daily security scans                                |






