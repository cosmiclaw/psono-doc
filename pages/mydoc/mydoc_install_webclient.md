---
title: Install Psono Webclient
summary: "Installation guide of the Psono Webclient"
tags: [installation, client]
sidebar: mydoc_sidebar
permalink: mydoc_install_webclient.html
folder: mydoc
---

## Preamble

The webclient is a stateless website, allowing users to access their passwords without the need to install anything
(besides a browser). It can be served by any web server, we prefer Nginx, but any other webserver (Apache, IIS) is fine too.
The web client is optional. As an alternative users can use the official Psono browser extensions.

## Installation with Docker

The latest build of our Web Client as a docker image can be found here: [hub.docker.com/r/psono/psono-client/](https://hub.docker.com/r/psono/psono-client/)
Follow belows instructions to bring it online.

1.  Create a `config.json` with the following content:
    
	```json
	{
	  "backend_servers": [{
	    "title": "Psono.pw",
	    "url": "https://www.psono.pw/server"
	  }],
	  "base_url": "https://www.psono.pw/",
	  "allow_custom_server": true
	}
	```
		
	Adjust the title and URLs according to your setup. The backend_server url should be the url where you see
	
	{"detail":"Authentication credentials were not provided."}

2.  (optional) Create privacy policy

	Create an own privacy policy as html  in e.g. `/opt/docker/psono-client/privacy-policy-content.html`
	
	If you are looking for inspiration you can check out our [privacy-policy-content.html](https://gitlab.com/psono/psono-client/blob/develop/src/common/data/privacy-policy-content.html)

3.  Run the docker image and expose the port

        docker run --name psono-client \
            -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
            -v /opt/docker/psono-client/privacy-policy-content.html:/usr/share/nginx/html/privacy-policy-content.html \
            -d --restart=unless-stopped -p 10101:80 psono/psono-client:latest

	This will now start the psono client on port 10101 with your config.json

    If you open now http://your-ip:10101 you should see a beautiful login screen.
    If not, please make sure you have no firewall on the server blocking you.
    
    {% include note.html content="Leave out the line with the privacy policy if you have no own" %}

4.  Setup nginx (or apache) relay

	A good webserver config is essential for your security. If you have chosen nginx as your webserver, then a suitable config can be found here:
	    
	    server {
	        ...
	        location ~* \.(?:ico|css|js|gif|jpe?g|png)$ {
	            expires                 30d;
	            add_header              Pragma public;
	            add_header              Cache-Control "public";
	            proxy_set_header        Host $host;
	            proxy_set_header        X-Real-IP $remote_addr;
	            proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
	            proxy_set_header        X-Forwarded-Proto $scheme;
	            
	            proxy_pass              http://localhost:10101;
	            proxy_redirect          http://localhost:10101 https://example.com;
	        }
	    
	        location / {
	            proxy_set_header        Host $host;
	            proxy_set_header        X-Real-IP $remote_addr;
	            proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
	            proxy_set_header        X-Forwarded-Proto $scheme;
	            
	            proxy_read_timeout      90;
	            
	            proxy_pass              http://localhost:10101;
	            proxy_redirect          http://localhost:10101 https://example.com;
	        }
	    }

	This config assumes that the webclient is running on localhost port 10101.
	
5.  Setup Reverse Proxy

	To run the Psono password manager in production, a reverse proxy is needed, to handle the ssl offloading and glue the psono server
	and webclient together. Follow the [guide to setup reverse proxy](mydoc_install_reverse_proxy.html) as a next step.
	
## Installation without Docker

The webclient is a pure html / js website, that can be hosted with any webserver and has zero dependencies.

1.  Download the webclient artifact

	Visit the following url and download the webclient:
	
	[psono.jfrog.io/psono/psono/client/latest/webclient.zip](https://psono.jfrog.io/psono/psono/client/latest/webclient.zip)
	
2.  Install webclient

    Unpack the webclient into the htdocs folder of your webserver.
   
3.  Update `config.json` with the following content:

	```json
	{
	  "backend_servers": [{
	    "title": "Psono.pw",
	    "url": "https://www.psono.pw/server"
	  }],
	  "base_url": "https://www.psono.pw/",
	  "allow_custom_server": true
	}
	```
		
	Adjust the title and URLs according to your setup. The backend_server url should be the url where you see
	
		{"detail":"Authentication credentials were not provided."}
		
5.  (optional) Update privacy policy

	Update your privacy policy in `privacy-policy-content.html`
	
6.  Setup Reverse Proxy

	To run the Psono password manager in production, a reverse proxy is needed, to handle the ssl offloading and glue the psono server
	and webclient together. Follow the [guide to setup reverse proxy](mydoc_install_reverse_proxy.html) as a next step.

{% include links.html %}
