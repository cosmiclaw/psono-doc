---
title: Features for Admins
last_updated: Aug 20th, 2017
summary: "If you're not sure whether Psono will support your requirements, this list provides a semi-comprehensive overview of available features."
published: true
sidebar: mydoc_sidebar
permalink: mydoc_overview_supported_features.html
folder: mydoc
---

Before you get into exploring Psono as a potential password manager, you may be wondering if it supports some basic
features needed to fulfill your requirements. The following table shows what is supported in Psono.

If you are looking more for features for users, then you can find them here in our [Features for Users](user_overview_supported_features.html)

## Features for administrators

Features | Supported | Notes
--------|-----------|-----------
Healthcheck | Yes | An API endpoint to monitor the health of your installation, including DB connectivity, time sync and various other
Prevent old passwords | Yes | Old passwords can be blocked without compromises for the "never send the password to the server"
Restrict 2FA methods | Yes | If administrators only want to allow particular 2FA methods, then they can restrict them.
Search by email address | Yes | By default the server does not allow to search users by their email address, but in a corporate environment administrators can enable this.
Search by partial usernames | Yes | By default the server does not allow to search users by parts of their username, but in a corporate environment administrators can enable this.
Admin Dashboard | Yes | Psono offers with the "Admin Client" a nice web based dashboard to manage users and their accounts.
Console Commands | Yes | Allows to automated processes like user generation or promotion of users with scripts.
Broad OS Support | Yes | Psono can run on any system that can provide a docker environment, e.g. AWS, Azure, GCE, Mac, Windows, Debian, Ubuntu, CentOS, Fedora, RHEL, SUSE, Oracle Linux
Vertical Scalability | Yes | Psono architecture allows vertical scalability and is only limited by the amount of write operations a single Postgres can handle (which is alot with the right hardware).

## Features exclusively in the Enterprise Edition

Features | Supported | Notes
--------|-----------|-----------
LDAP Authentication* | Yes | Allows to login with their LDAP credentials
LDAP Groups* | Yes | Adds users automatically groups based on their LDAP groups
Audit Logging | Yes | Once enabled the server will log all events. Administrators can control what they are interested with white and blacklists.
2FA Enforcement | Yes | Users can be forced to provide a second factor before they can use Psono.

\* LDAP comes with some requirements, e.g. that the server needs to be able to provide the users plaintext password or that the
server needs a way to re-encrypt the users secrets when he is changing his password on the LDAP server.
Using this feature therefore necessarily will tell the client to send the plaintext password. (A warning is displayed to
the user before that happens, to prevent any missuse of that feature.) and the server will store the users secrets (not
his password) encrypted in the database to be able to re-encrypt them if necessary.

{% include note.html content="If you are interested in a feature that is not listed, then let us know." %}


{% include links.html %}
