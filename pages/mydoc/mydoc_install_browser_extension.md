---
title: Install Browser Extension
tags: [installation, client]
summary: "Installation guide for the Psono browser extensions for Chrome or Firefox"
sidebar: mydoc_sidebar
permalink: mydoc_install_browser_extension.html
folder: mydoc
---

To install one of our browser extensions, follow the appropriate guide below.

## Install Chrome extension

- Open your Chrome browser
- Visit the following url:

[chrome.google.com/webstore/detail/psonopw/eljmjmgjkbmpmfljlmklcfineebidmlo](https://chrome.google.com/webstore/detail/psonopw/eljmjmgjkbmpmfljlmklcfineebidmlo)

## Install Firefox extension

- Open your Firefox browser
- Visit the following url:

[addons.mozilla.org/de/firefox/addon/psono-pw-password-manager/](https://addons.mozilla.org/de/firefox/addon/psono-pw-password-manager/)


{% include links.html %}

