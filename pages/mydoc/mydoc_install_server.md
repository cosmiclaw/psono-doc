---
title: Install Psono Server CE
tags: [installation, server]
summary: "Install instruction for the Community Edition of the Psono server"
sidebar: mydoc_sidebar
permalink: mydoc_install_server.html
folder: mydoc
---

## Preamble

At this point we assume that you already have a postgres database running, ready for connections. If not follow this
[guide to setup postgres](mydoc_install_postgres.html).

## Installation with Docker

1.  Create a settings.yaml in e.g. /opt/docker/psono/ with the following content
    
	```yaml
	# generate the following six parameters with the following command
    # docker run --rm -ti psono/psono-server:latest python3 ./psono/manage.py generateserverkeys
	SECRET_KEY: 'SOME SUPER SECRET KEY THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
	ACTIVATION_LINK_SECRET: 'SOME SUPER SECRET ACTIVATION LINK SECRET THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
	DB_SECRET: 'SOME SUPER SECRET DB SECRET THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
	EMAIL_SECRET_SALT: '$2b$12$XUG.sKxC2jmkUvWQjg53.e'
	PRIVATE_KEY: '302650c3c82f7111c2e8ceb660d32173cdc8c3d7717f1d4f982aad5234648fcb'
	PUBLIC_KEY: '02da2ad857321d701d754a7e60d0a147cdbc400ff4465e1f57bc2d9fbfeddf0b'

	# The URL of the web client (path to e.g activate.html without the trailing slash)
	# WEB_CLIENT_URL: 'https://www.psono.pw'

	# Switch DEBUG to false if you go into production
	DEBUG: False

	# Adjust this according to Django Documentation https://docs.djangoproject.com/en/1.10/ref/settings/
	ALLOWED_HOSTS: ['*']

	# Should be your domain without "www.". Will be the last part of the username
	ALLOWED_DOMAINS: ['psono.pw']

	# If you want to disable registration, you can comment in the following line
	# ALLOW_REGISTRATION: False

	# If you want to restrict registration to some email addresses you can specify here a list of domains to filter
	# REGISTRATION_EMAIL_FILTER: ['company1.com', 'company2.com']

	# Should be the URL of the host under which the host is reachable
	# If you open the url you should have a text similar to {"detail":"Authentication credentials were not provided."}
	HOST_URL: 'https://www.psono.pw/server'

	# The email used to send emails, e.g. for activation
	EMAIL_FROM: 'the-mail-for-for-example-useraccount-activations@test.com'
	EMAIL_HOST: 'localhost'
	EMAIL_HOST_USER: ''
	EMAIL_HOST_PASSWORD : ''
	EMAIL_PORT: 25
	EMAIL_SUBJECT_PREFIX: ''
	EMAIL_USE_TLS: False
	EMAIL_USE_SSL: False
	EMAIL_SSL_CERTFILE:
	EMAIL_SSL_KEYFILE:
	EMAIL_TIMEOUT:

	# In case one wants to use mailgun, comment in below lines and provide the mailgun access key and server name
	# EMAIL_BACKEND: 'django_mailgun.MailgunBackend'
	# MAILGUN_ACCESS_KEY: ''
	# MAILGUN_SERVER_NAME: ''

	# In case you want to offer Yubikey support, create a pair of credentials here https://upgrade.yubico.com/getapikey/
	# and update the following two lines before commenting them in
	# YUBIKEY_CLIENT_ID: '123456'
	# YUBIKEY_SECRET_KEY: '8I65IA6ASDFIUHGIH5021FKJA='

	# If you have own Yubico servers, you can specify here the urls as a list
    # YUBICO_API_URLS: ['https://api.yubico.com/wsapi/2.0/verify']

	# Cache enabled without belows Redis may lead to unexpected behaviour

	# Cache with Redis
	# By default you should use something different than database 0 or 1, e.g. 13 (default max is 16, can be configured in
	# redis.conf) possible URLS are:
	#    redis://[:password]@localhost:6379/0
	#    rediss://[:password]@localhost:6379/0
	#    unix://[:password]@/path/to/socket.sock?db=0
	# CACHE_ENABLE: False
	# CACHE_REDIS: False
	# CACHE_REDIS_LOCATION: 'redis://127.0.0.1:6379/13'

	# Disables Throttling (necessary for unittests to pass) by overriding the cache with a dummy cache
	# https://docs.djangoproject.com/en/1.11/topics/cache/#dummy-caching-for-development
	# THROTTLING: False

	# Enables the management API, required for the psono-admin-client / admin portal
	# MANAGEMENT_ENABLED: False

	# Allows that users can search for partial usernames
	# ALLOW_USER_SEARCH_BY_USERNAME_PARTIAL: True

	# Allows that users can search for email addresses too
	# ALLOW_USER_SEARCH_BY_EMAIL: True

	# Only necessary if the psono-client runs on a sub path (no trailing slash) e.g. "https://wwww.psono.pw"
	# WEB_CLIENT_URL: ''

	# Prevents the use of the last X passwords. 0 disables it.
	# DISABLE_LAST_PASSWORDS: 0

	# Your Postgres Database credentials
    DATABASES:
        default:
            'ENGINE': 'django.db.backends.postgresql_psycopg2'
            'NAME': 'psono'
            'USER': 'psono'
            'PASSWORD': 'password'
            'HOST': 'localhost'
            'PORT': '5432'
	# for master / slave replication setup comment in the following (all reads will be redirected to the slave
	#    slave:
	#        'ENGINE': 'django.db.backends.postgresql_psycopg2'
	#        'NAME': 'YourPostgresDatabase'
	#        'USER': 'YourPostgresUser'
	#        'PASSWORD': 'YourPostgresPassword'
	#        'HOST': 'YourPostgresHost'
	#        'PORT': 'YourPostgresPort'

	# The path to the template folder can be "shadowed" if required later
	TEMPLATES: [
	    {
	        'BACKEND': 'django.template.backends.django.DjangoTemplates',
	        'DIRS': ['/root/psono/templates'],
	        'APP_DIRS': True,
	        'OPTIONS': {
	            'context_processors': [
	                'django.template.context_processors.debug',
	                'django.template.context_processors.request',
	                'django.contrib.auth.context_processors.auth',
	                'django.contrib.messages.context_processors.messages',
	            ],
	        },
	    },
	]
	```

    {% include note.html content="Update database credentials / secrets / paths like described in the comments" %}

2.  Test E-Mail

    The most tedious step is usually for me to get e-mail working.
    To make this step easier, we offer a small test script which will
    send a test e-mail.
    
    To send a test e-mail to `something@something.com` execute:

        docker run --rm \
          -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
          -ti psono/psono-server:latest python3 ./psono/manage.py sendtestmail something@something.com

    If you receive this test e-mail, e-mail should be configured proper.
    
3.  Prepare the database

        docker run --rm \
          -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
          -ti psono/psono-server:latest python3 ./psono/manage.py migrate
    
4.  Run the dockered psono server image and expose the server port

        docker run --name psono-server \
            --sysctl net.core.somaxconn=65535 \
            -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
            -d --restart=unless-stopped -p 10100:80 psono/psono-server:latest
            
    This will start the Psono server on port 10100. 
    If you open now http://your-ip:10100 you should see something like this:

        {"detail":"Authentication credentials were not provided."}

    If not, please make sure you have no firewall on the server blocking you.
	
5.  Setup cleanup job

    Execute the following command:

	    crontab -e
	    
	and add the following line:
	
        30 2 * * * docker run --rm -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml -ti psono/psono-server:latest python3 ./psono/manage.py cleartoken >> /var/log/cron.log 2>&1
	
6.  Setup Reverse Proxy

	To run the Psono password manager in production, a reverse proxy is needed, to handle the ssl offloading and glue the psono server
	and webclient together. Follow the [guide to setup reverse proxy](mydoc_install_reverse_proxy.html) as a next step.

## Installation for Ubuntu

This guide will install the Psono server, and runs it with uwsgi and nginx. It has been tested on Ubuntu 16.04.

1.  Become root

        sudo su

2.  Install some generic stuff

        apt-get update
        apt-get install -y \
                git \
		        libyaml-dev \
		        libpython3-dev \
		        libpq-dev \
		        libffi-dev \
		        python3-dev \
		        python-pip \
		        python3-pip \
		        python3-psycopg2 \
		        postgresql-client \
		        haveged \
		        libsasl2-dev \
		        libldap2-dev \
		        libssl-dev \
                supervisor
        pip3 install --upgrade pip
        pip3 install gunicorn

3.  Create psono user

        adduser psono
        
4.  Become the psono user

        su psono

5.  Clone git repository

        git clone https://gitlab.com/psono/psono-server.git ~/psono-server

6.  Install python requirements
        
        Ctrl + D                            # become root again
        cd /home/psono/psono-server
        pip3 install -r requirements.txt
        su psono                            # become psono again
        
7.  Create settings folder

        mkdir ~/.psono_server
    
8.  Create a settings.yaml in ~/.psono_server/ with the following content
        
    ```yaml
    # generate the following six parameters with the following command
    # python3 ~/psono-server/psono/manage.py generateserverkeys
    SECRET_KEY: 'SOME SUPER SECRET KEY THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
    ACTIVATION_LINK_SECRET: 'SOME SUPER SECRET ACTIVATION LINK SECRET THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
    DB_SECRET: 'SOME SUPER SECRET DB SECRET THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
    EMAIL_SECRET_SALT: '$2b$12$XUG.sKxC2jmkUvWQjg53.e'
    PRIVATE_KEY: '302650c3c82f7111c2e8ceb660d32173cdc8c3d7717f1d4f982aad5234648fcb'
    PUBLIC_KEY: '02da2ad857321d701d754a7e60d0a147cdbc400ff4465e1f57bc2d9fbfeddf0b'

    # The URL of the web client (path to e.g activate.html without the trailing slash)
    # WEB_CLIENT_URL: 'https://www.psono.pw'

    # Switch DEBUG to false if you go into production
    DEBUG: False

    # Adjust this according to Django Documentation https://docs.djangoproject.com/en/1.10/ref/settings/
    ALLOWED_HOSTS: ['*']

    # Should be your domain without "www.". Will be the last part of the username
    ALLOWED_DOMAINS: ['psono.pw']

	# If you want to disable registration, you can comment in the following line
	# ALLOW_REGISTRATION: False

	# If you want to restrict registration to some email addresses you can specify here a list of domains to filter
	# REGISTRATION_EMAIL_FILTER: ['company1.com', 'company2.com']

    # Should be the URL of the host under which the host is reachable
    # If you open the url you should have a text similar to {"detail":"Authentication credentials were not provided."}
    HOST_URL: 'https://www.psono.pw/server'

    # The email used to send emails, e.g. for activation
    EMAIL_FROM: 'the-mail-for-for-example-useraccount-activations@test.com'
    EMAIL_HOST: 'localhost'
    EMAIL_HOST_USER: ''
    EMAIL_HOST_PASSWORD : ''
    EMAIL_PORT: 25
    EMAIL_SUBJECT_PREFIX: ''
    EMAIL_USE_TLS: False
    EMAIL_USE_SSL: False
    EMAIL_SSL_CERTFILE:
    EMAIL_SSL_KEYFILE:
    EMAIL_TIMEOUT:

    # In case one wants to use mailgun, comment in below lines and provide the mailgun access key and server name
    # EMAIL_BACKEND: 'django_mailgun.MailgunBackend'
    # MAILGUN_ACCESS_KEY: ''
    # MAILGUN_SERVER_NAME: ''

    # In case you want to offer Yubikey support, create a pair of credentials here https://upgrade.yubico.com/getapikey/
    # and update the following two lines before commenting them in
    # YUBIKEY_CLIENT_ID: '123456'
    # YUBIKEY_SECRET_KEY: '8I65IA6ASDFIUHGIH5021FKJA='

	# If you have own Yubico servers, you can specify here the urls as a list
    # YUBICO_API_URLS: ['https://api.yubico.com/wsapi/2.0/verify']

    # Cache enabled without belows Redis may lead to unexpected behaviour

    # Cache with Redis
    # By default you should use something different than database 0 or 1, e.g. 13 (default max is 16, can be configured in
    # redis.conf) possible URLS are:
    #    redis://[:password]@localhost:6379/0
    #    rediss://[:password]@localhost:6379/0
    #    unix://[:password]@/path/to/socket.sock?db=0
    # CACHE_ENABLE: False
    # CACHE_REDIS: False
    # CACHE_REDIS_LOCATION: 'redis://127.0.0.1:6379/13'

    # Disables Throttling (necessary for unittests to pass) by overriding the cache with a dummy cache
    # https://docs.djangoproject.com/en/1.11/topics/cache/#dummy-caching-for-development
    # THROTTLING: False

	# Enables the management API, required for the psono-admin-client / admin portal
	# MANAGEMENT_ENABLED: False

	# Allows that users can search for partial usernames
	# ALLOW_USER_SEARCH_BY_USERNAME_PARTIAL: True

	# Allows that users can search for email addresses too
	# ALLOW_USER_SEARCH_BY_EMAIL: True

	# Only necessary if the psono-client runs on a sub path (no trailing slash) e.g. "https://wwww.psono.pw"
	# WEB_CLIENT_URL: ''

	# Prevents the use of the last X passwords. 0 disables it.
	# DISABLE_LAST_PASSWORDS: 0

    # Your Postgres Database credentials
    DATABASES:
        default:
            'ENGINE': 'django.db.backends.postgresql_psycopg2'
            'NAME': 'psono'
            'USER': 'psono'
            'PASSWORD': 'password'
            'HOST': 'localhost'
            'PORT': '5432'
    # for master / slave replication setup comment in the following (all reads will be redirected to the slave
    #    slave:
    #        'ENGINE': 'django.db.backends.postgresql_psycopg2'
    #        'NAME': 'YourPostgresDatabase'
    #        'USER': 'YourPostgresUser'
    #        'PASSWORD': 'YourPostgresPassword'
    #        'HOST': 'YourPostgresHost'
    #        'PORT': 'YourPostgresPort'

    # Update the path to your templates folder
    # If you do not want to change it (yet) you can leave it like it is.
    TEMPLATES: [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': ['/home/psono/psono-server/psono/templates'],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
    ]
    ```
    
    {% include note.html content="Update database credentials / secrets / paths like described in the comments" %}

    
9.  Test E-Mail

    The most tedious step is usually for me to get e-mail working.
    To make this step easier, we offer a small test script which will
    send a test e-mail.
    
    To send a test e-mail to `something@something.com` execute:

        python3 ~/psono-server/psono/manage.py sendtestmail something@something.com

    If you receive this test e-mail, e-mail should be configured proper.
    
10. Create our database

        python3  ~/psono-server/psono/manage.py migrate
    
11. Run the psono server

		cd ~/psono-server/psono
        gunicorn --bind 0.0.0.0:10100 psono.wsgi
            
    This will start the Psono server on port 10100. 
    If you open now http://your-ip:10100 you should see something like this:

        {"detail":"Authentication credentials were not provided."}

    If not, please make sure you have no firewall on the server blocking you.

12. Become root again

        Ctrl + D

12. Create supervisor config

    Create a psono-server.conf in /etc/supervisor/conf.d/ with the following content:
    
    ```
    [program:psono-server]
    command = /usr/local/bin/gunicorn --bind 127.0.0.1:10100 psono.wsgi
    directory=/home/psono/psono-server/psono
    user = psono
    autostart=true
    autorestart=true
    redirect_stderr=true
	```
	
	{% include note.html content="You may have realised that we changed the bind. This way Psono is only accessible from localhost, which is fine as we will proxy requests with nginx" %}


13. Reload supervisorctl

        supervisorctl reload

	Now you can control the Psono server with supervisorctrl commands e.g.
	
	* supervisorctl status psono-server
	* supervisorctl start psono-server
	* supervisorctl stop psono-server
	
14. Setup cleanup job

    Execute the following command:

	    crontab -e
	    
	and add the following line:
	
        30 2 * * * psono python3 /home/psono/psono-server/psono/manage.py cleartoken >> /var/log/cron.log 2>&1
	
15. Setup Reverse Proxy

	To run the Psono password manager in production, a reverse proxy is needed, to handle the ssl offloading and glue the psono server
	and webclient together. Follow the [guide to setup reverse proxy](mydoc_install_reverse_proxy.html) as a next step.

## Installation for CentOS

This guide will install the Psono server, and runs it with uwsgi and nginx. It has been tested on CentOS 7.

1.  Become root

        sudo su

2.  Install some generic stuff

        yum -y update
        yum -y install epel-release
        yum -y update
        yum -y install \
                git \
	            gcc \
                openssl-devel \
                libffi-devel \
                python34 \
                python34-devel \
                python34-pip \
                postgresql \
                postgresql-devel \
                postgresql-client \
                haveged \
                openldap-devel \
	            supervisor
        pip3 install --upgrade pip
        pip3 install gunicorn

3.  Create psono user

        adduser psono
        
4.  Become the psono user

        su psono

5.  Clone git repository

        git clone https://gitlab.com/psono/psono-server.git ~/psono-server

6.  Install python requirements
        
        Ctrl + D                            # become root again
        cd /home/psono/psono-server
        pip3 install -r requirements.txt
        su psono                            # become psono again
        
7.  Create settings folder

        mkdir ~/.psono_server
    
8.  Create a settings.yaml in ~/.psono_server/ with the following content
        
    ```yaml
    # generate the following six parameters with the following command
    # python3 ~/psono-server/psono/manage.py generateserverkeys
    SECRET_KEY: 'SOME SUPER SECRET KEY THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
    ACTIVATION_LINK_SECRET: 'SOME SUPER SECRET ACTIVATION LINK SECRET THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
    DB_SECRET: 'SOME SUPER SECRET DB SECRET THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
    EMAIL_SECRET_SALT: '$2b$12$XUG.sKxC2jmkUvWQjg53.e'
    PRIVATE_KEY: '302650c3c82f7111c2e8ceb660d32173cdc8c3d7717f1d4f982aad5234648fcb'
    PUBLIC_KEY: '02da2ad857321d701d754a7e60d0a147cdbc400ff4465e1f57bc2d9fbfeddf0b'

    # The URL of the web client (path to e.g activate.html without the trailing slash)
    # WEB_CLIENT_URL: 'https://www.psono.pw'

    # Switch DEBUG to false if you go into production
    DEBUG: False

    # Adjust this according to Django Documentation https://docs.djangoproject.com/en/1.10/ref/settings/
    ALLOWED_HOSTS: ['*']

    # Should be your domain without "www.". Will be the last part of the username
    ALLOWED_DOMAINS: ['psono.pw']

	# If you want to disable registration, you can comment in the following line
	# ALLOW_REGISTRATION: False

	# If you want to restrict registration to some email addresses you can specify here a list of domains to filter
	# REGISTRATION_EMAIL_FILTER: ['company1.com', 'company2.com']

    # Should be the URL of the host under which the host is reachable
    # If you open the url you should have a text similar to {"detail":"Authentication credentials were not provided."}
    HOST_URL: 'https://www.psono.pw/server'

    # The email used to send emails, e.g. for activation
    EMAIL_FROM: 'the-mail-for-for-example-useraccount-activations@test.com'
    EMAIL_HOST: 'localhost'
    EMAIL_HOST_USER: ''
    EMAIL_HOST_PASSWORD : ''
    EMAIL_PORT: 25
    EMAIL_SUBJECT_PREFIX: ''
    EMAIL_USE_TLS: False
    EMAIL_USE_SSL: False
    EMAIL_SSL_CERTFILE:
    EMAIL_SSL_KEYFILE:
    EMAIL_TIMEOUT:

    # In case one wants to use mailgun, comment in below lines and provide the mailgun access key and server name
    # EMAIL_BACKEND: 'django_mailgun.MailgunBackend'
    # MAILGUN_ACCESS_KEY: ''
    # MAILGUN_SERVER_NAME: ''

    # In case you want to offer Yubikey support, create a pair of credentials here https://upgrade.yubico.com/getapikey/
    # and update the following two lines before commenting them in
    # YUBIKEY_CLIENT_ID: '123456'
    # YUBIKEY_SECRET_KEY: '8I65IA6ASDFIUHGIH5021FKJA='

	# If you have own Yubico servers, you can specify here the urls as a list
    # YUBICO_API_URLS: ['https://api.yubico.com/wsapi/2.0/verify']

    # Cache enabled without belows Redis may lead to unexpected behaviour

    # Cache with Redis
    # By default you should use something different than database 0 or 1, e.g. 13 (default max is 16, can be configured in
    # redis.conf) possible URLS are:
    #    redis://[:password]@localhost:6379/0
    #    rediss://[:password]@localhost:6379/0
    #    unix://[:password]@/path/to/socket.sock?db=0
    # CACHE_ENABLE: False
    # CACHE_REDIS: False
    # CACHE_REDIS_LOCATION: 'redis://127.0.0.1:6379/13'

    # Disables Throttling (necessary for unittests to pass) by overriding the cache with a dummy cache
    # https://docs.djangoproject.com/en/1.11/topics/cache/#dummy-caching-for-development
    # THROTTLING: False

	# Enables the management API, required for the psono-admin-client / admin portal
	# MANAGEMENT_ENABLED: False

	# Allows that users can search for partial usernames
	# ALLOW_USER_SEARCH_BY_USERNAME_PARTIAL: True

	# Allows that users can search for email addresses too
	# ALLOW_USER_SEARCH_BY_EMAIL: True

	# Only necessary if the psono-client runs on a sub path (no trailing slash) e.g. "https://wwww.psono.pw"
	# WEB_CLIENT_URL: ''

	# Prevents the use of the last X passwords. 0 disables it.
	# DISABLE_LAST_PASSWORDS: 0

    # Your Postgres Database credentials
    DATABASES:
        default:
            'ENGINE': 'django.db.backends.postgresql_psycopg2'
            'NAME': 'psono'
            'USER': 'psono'
            'PASSWORD': 'password'
            'HOST': 'localhost'
            'PORT': '5432'
    # for master / slave replication setup comment in the following (all reads will be redirected to the slave
    #    slave:
    #        'ENGINE': 'django.db.backends.postgresql_psycopg2'
    #        'NAME': 'YourPostgresDatabase'
    #        'USER': 'YourPostgresUser'
    #        'PASSWORD': 'YourPostgresPassword'
    #        'HOST': 'YourPostgresHost'
    #        'PORT': 'YourPostgresPort'

    # Update the path to your templates folder
    # If you do not want to change it (yet) you can leave it like it is.
    TEMPLATES: [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': ['/home/psono/psono-server/psono/templates'],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
    ]
    ```
    
    {% include note.html content="Update database credentials / secrets / paths like described in the comments" %}

    
9.  Test E-Mail

    The most tedious step is usually for me to get e-mail working.
    To make this step easier, we offer a small test script which will
    send a test e-mail.
    
    To send a test e-mail to `something@something.com` execute:

        python3 ~/psono-server/psono/manage.py sendtestmail something@something.com

    If you receive this test e-mail, e-mail should be configured proper.
    
10. Create our database

        python3  ~/psono-server/psono/manage.py migrate
    
11. Run the psono server
		
		cd ~/psono-server/psono
        gunicorn --bind 0.0.0.0:10100 psono.wsgi
            
    This will start the Psono server on port 10100. 
    If you open now http://your-ip:10100 you should see something like this:

        {"detail":"Authentication credentials were not provided."}

    If not, please make sure you have no firewall on the server blocking you.

12. Become root again

        Ctrl + D

12. Create supervisor config

    Create a psono-server.ini in /etc/supervisord.d/ with the following content:
    
    ```
    [program:psono-server]
    command = /usr/bin/gunicorn --bind 127.0.0.1:10100 psono.wsgi
    directory=/home/psono/psono-server/psono
    user = psono
    autostart=true
    autorestart=true
    redirect_stderr=true
	```
	
	{% include note.html content="You may have realised that we changed the bind. This way Psono is only accessible from localhost, which is fine as we will proxy requests with nginx" %}

13. Start supervisord and register autostart

	systemctl start supervisord
	systemctl enable supervisord

	Now you can control the Psono server with supervisorctrl commands e.g.
	
	* supervisorctl status psono-server
	* supervisorctl start psono-server
	* supervisorctl stop psono-server
	
14. Setup cleanup job

    Execute the following command:

	    crontab -e
	    
	and add the following line:
	
        30 2 * * * psono python3 /home/psono/psono-server/psono/manage.py cleartoken >> /var/log/cron.log 2>&1
	
15. Setup Reverse Proxy

	To run the Psono password manager in production, a reverse proxy is needed, to handle the ssl offloading and glue the psono server
	and webclient together. Follow the [guide to setup reverse proxy](mydoc_install_reverse_proxy.html) as a next step.


## Note: Installation behind Firewall

If you have put your installation behind a firewall, you have to whitelist some ports / adjust some settings, that all
features work:

* Outgoing TCP / UDP 123 connection to time.google.com: Psono requires a synced time for various reasons (Google Authenticator, YubiKey, Throttling, Replay protection, ...)
Therefore it has a healthcheck, to compare the local time to a time server (by default time.google.com). You can specify your own timeserver in the settings.yaml with the "TIME_SERVER" parameter.
* Outgoing TCP 443 connection to api*.yubico.com: Psono validates YubiKey OTP tokens against the official Yubikey Servers. You can use an own YubiKey server with the "YUBICO_API_URLS" parameter in the settings.yaml
* Outgoing TCP 443 connection to api*.duosecurity.com: Psono initiates connection a to the Duo api server, whenever someone uses Duo's 2 Factor authentication.

{% include links.html %}
