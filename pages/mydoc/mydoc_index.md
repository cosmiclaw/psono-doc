---
title: Admin Documentation of Psono
sidebar: mydoc_sidebar
permalink: mydoc_index.html
summary: These brief instructions will help you get started quickly with the psono password server. The other topics in this help provide additional information and detail about working with other aspects.
---

If you are a no admin and are just looking for some user documentation, then please follow [this link here](index.html) or use the "Documentation Type" link at the top to switch.

## Overview

The Psono password manager consists of / requires multiple components:

1.  Postgres Database

    Our main datastore that holds all data. Postgres in version 9.6 is supported, but all 9.x
    versions should work.

2.  Psono Server

    The "core" of the system, doing all the logic. Exposing a REST interface for all clients to access.
    Can be installed bare metal or with a docker image.

3.  Psono Webclient

    The webclient allows clients to access their passwords without any installation.
    The webclient consists of no backend logic and runs completely in your browser with html and JavaScript.

4.  Reverse Proxy

    The reverse Proxy glues the server and client together and is responsible for the SSL offloading.
    Currently supported is nginx.

5.  E-Mail Server

    Required to send registration emails. A wide range of different e-mail servers is supported.

6.  Browser extensions

    Browser extensions are basically bundled versions of our webclient which extend the functionality with useful
    features, like password capture, form fill, quick search and so on.

7.  Cache (optional)

    Different caches are supported, e.g. Redis, memcache, local memory
    More infos can be found here:
    [https://docs.djangoproject.com/en/1.11/topics/cache/](https://docs.djangoproject.com/en/1.11/topics/cache/)

## Typical Setup

A typical Psono Setup looks like this:

![Overview of a typical setup](images/setup_typical.png)

## Advanced Setup

A more "advanced" Psono Setup with HA, a scaled Postgres Database with failover, a shared cache and an admin panel behind
a VPN tunnel and a dedicated management instance, could look like this:

![Overview of a typical setup](images/setup_advanced.png)


{% include links.html %}
