---
title: Update Psono Server CE
summary: "Update instruction for the Community Edition of the Psono server"
tags: [update, server]
sidebar: mydoc_sidebar
permalink: mydoc_update_server.html
folder: mydoc
---

## Preamble

From time to time you want to update to the latest version. Dependent on the installation you will have to follow
different (but similar) procedures. At this point we assume that you have created a backup of your postgres database.
If you have a possibility to snapshot your machine, please do so. The guide contains some steps to backup the application
yet due to updating some system dependencies / python packages on the way can fail in some cases.

## Update with Docker

1.  Update the docker image

	    docker pull psono/psono-server:latest
    
2.  Update the database

        docker run --rm \
          -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
          -ti psono/psono-server:latest python3 ./psono/manage.py migrate
    
2.  Stop old psono-server

        docker stop psono-server
    
3.  Start new psono-server

        docker run --name psono-server-new \
            --sysctl net.core.somaxconn=65535 \
            -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
            -d --restart=unless-stopped -p 10100:80 psono/psono-server:latest
            
4.  Cleanup

	If everything works you can cleanup your containers with the following commands:
	
		docker rm psono-server
		docker rename psono-server-new psono-server

If anything fails you should be able to restore the database, and start the old docker container again. 
            
## Update for Ubuntu

This guide will update the Psono server. It has been tested on Ubuntu 16.04.

1.  Become the psono user

        sudo su psono
        
2.  Backup the git folder

        cp ~/psono-server ~/psono-server-backup

3.  Clone the updated newest version

        git clone https://gitlab.com/psono/psono-server.git ~/psono-server

4.  Become root

        Ctrl + D
        sudo su

5.  Install python requirements
        
        cd /home/psono/psono-server
        pip3 install -r requirements.txt
        su psono                            # become psono again
    
6.  Update your database

        python3  ~/psono-server/psono/manage.py migrate
    

7.  Restart the psono server

		supervisorctl restart psono-server
		
8.  Cleanup

	If everything works you can remove the backup folder
	
		sudo rm /home/psono/psono-server-backup

If anything fails you should be able to restore the database, delete the new psono-server folder and copy the backup
folder back to its original place. 

## Update for CentOS

This guide will update the Psono server. It has been tested on CentOS 7.

1.  Become the psono user

        sudo su psono
        
2.  Backup the git folder

        cp ~/psono-server ~/psono-server-backup

3.  Clone the updated newest version

        git clone https://gitlab.com/psono/psono-server.git ~/psono-server

4.  Become root

        Ctrl + D
        sudo su

5.  Install python requirements
        
        cd /home/psono/psono-server
        pip3 install -r requirements.txt
        su psono                            # become psono again
    
6.  Update your database

        python3  ~/psono-server/psono/manage.py migrate

7.  Restart the psono server

		supervisorctl restart psono-server
		
8.  Cleanup

	If everything works you can remove the backup folder
	
		sudo rm /home/psono/psono-server-backup

If anything fails you should be able to restore the database, delete the new psono-server folder and copy the backup
folder back to its original place. 
		
{% include links.html %}
