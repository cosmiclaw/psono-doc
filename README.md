# PSONO Documentation

# Canonical source

The canonical source of PSONO Server is [hosted on GitLab.com](https://gitlab.com/psono/psono-doc).

# Preamble

Psono docs are driven by Jekyll static site generator.

This whole guide is based on Ubuntu 16.04 LTS. Ubuntu 12.04+ LTS and Debian based systems should be similar if not even
identical.

# General

The current build can be found here:

https://doc.psono.com/

# Installation

To install ruby, gems and jekyll follow the instructions below

	sudo apt-get install ruby rubygems ruby-dev
	sudo gem install jekyll bundler
	bundle install
	
# Start the development server

To start the development server execute the following command:

	bundle exec jekyll serve

# Theme

The theme that we are using can be found here:

https://github.com/tomjohnson1492/documentation-theme-jekyll

# Diagrams & Libraries

All Diagrams have been developed with draw.io and are provided by xml specifications and graphics.
The used libraries can be found here https://github.com/jgraph/drawio-libs e.g. :

* https://jgraph.github.io/drawio-libs/libs/osa-icons.xml
* https://jgraph.github.io/drawio-libs/libs/font-awesome.xml

# LICENSE

Visit the [License.md](/LICENSE.md) for more details